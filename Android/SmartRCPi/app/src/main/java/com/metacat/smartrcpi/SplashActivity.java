package com.metacat.smartrcpi;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

public class SplashActivity extends AppCompatActivity {
    // Splash screen timer

    private static int SPLASH_TIME_OUT = 3000;
    private ImageView muscleCarImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        muscleCarImageView = (ImageView) findViewById(R.id.muscleCar);

        muscleCarImageView.setTranslationX(-1500);

        muscleCarImageView.animate().translationXBy(1500).setDuration(500);

        getWindow().setStatusBarColor(Color.TRANSPARENT); // make the Status Bar transparent
        getWindow().setNavigationBarColor(Color.TRANSPARENT); // make the Navi Bar transparent
        // Showing splash screen with a timer.
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // Start your application main_activity
                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(i);

                // Close this activity
                finish();
            }
        }, SPLASH_TIME_OUT); // Timer
    }
}