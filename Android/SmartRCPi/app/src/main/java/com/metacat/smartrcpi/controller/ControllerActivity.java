package com.metacat.smartrcpi.controller;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;

import com.google.gson.JsonObject;
import com.metacat.smartrcpi.R;
import com.metacat.smartrcpi.retrofit.RetroAPI;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.metacat.smartrcpi.wifi.SSHActivity.executeRemoteCommand;


public class ControllerActivity extends AppCompatActivity {

    private ImageButton btnAccelerate;
    private ImageButton btnDecelerate;
    private ImageButton btnLeft;
    private ImageButton btnRight;
    private Button btnStop;
    private ImageButton streamBtn;
    private WebView webView;
    private JsonObject jsonObject;
    private String msg;
    private long lastDown;
    private long lastDuration;

    private static final String START_STREAM = "rpiWebServer/./startStream.sh&";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.controller);

        new AsyncTask<Integer, Void, Void>(){
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Integer... params) {
                try {
                    executeRemoteCommand("pi",
                            "asdfasdf",
                            "10.0.1.9",
                            22,
                            START_STREAM);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                webView = (WebView) findViewById(R.id.webView);
                webView.getSettings().setLoadWithOverviewMode(true);
                webView.getSettings().setUseWideViewPort(true);

                webView.loadUrl("http://10.0.1.9:8080/?action=stream");
            }
        }.execute(1);

        btnAccelerate = (ImageButton) findViewById(R.id.buttonUp);

        btnDecelerate = (ImageButton) findViewById(R.id.buttonDown);

        btnLeft = (ImageButton) findViewById(R.id.buttonLeft);

        btnRight = (ImageButton) findViewById(R.id.buttonRight);

        btnStop = (Button) findViewById(R.id.stopButton);

        streamBtn = (ImageButton) findViewById(R.id.startStream);

//        mjpegView.setMode(MjpegView.MODE_FIT_WIDTH);
//        mjpegView.setAdjustHeight(true);
//        mjpegView.setUrl("http://www.example.com/mjpeg.php?id=1234");
//        mjpegView.startStream();

        streamBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                webView.stopLoading();
                webView.loadUrl("about:blank");
            }
        });

        btnAccelerate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    lastDown = System.currentTimeMillis();

                    jsonObject = new JsonObject();
                    jsonObject.addProperty("text", "motorUp");

                    RetroAPI.APIService service = RetroAPI.createService(RetroAPI.APIService.class);
                    service.getVectors(jsonObject)
                            .enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    if (response.isSuccessful()) {

                                        try {
                                            msg = response.body().string();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        System.out.println("---TTTT :: POST msg from server :: " + msg);
//                                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                                    }
                                }
                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    System.out.println("---TTTT :: POST Throwable EXCEPTION:: " + t.getMessage());
                                }
                            });
                }
                else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    lastDuration = System.currentTimeMillis() - lastDown;

                    jsonObject = new JsonObject();
                    jsonObject.addProperty("text", "muStop");

                    RetroAPI.APIService service = RetroAPI.createService(RetroAPI.APIService.class);
                    service.getVectors(jsonObject)
                            .enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    if (response.isSuccessful()) {

                                        try {
                                            msg = response.body().string();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        System.out.println("---TTTT :: POST msg from server :: " + msg);
//                                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                                    }
                                }
                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    System.out.println("---TTTT :: POST Throwable EXCEPTION:: " + t.getMessage());
                                }
                            });
                }
                return true;
            }

        });

        btnDecelerate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    lastDown = System.currentTimeMillis();

                    jsonObject = new JsonObject();
                    jsonObject.addProperty("text", "motorDown");

                    RetroAPI.APIService service = RetroAPI.createService(RetroAPI.APIService.class);
                    service.getVectors(jsonObject)
                            .enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    if (response.isSuccessful()) {

                                        try {
                                            msg = response.body().string();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        System.out.println("---TTTT :: POST msg from server :: " + msg);
//                                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                                    }
                                }
                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    System.out.println("---TTTT :: POST Throwable EXCEPTION:: " + t.getMessage());
                                }
                            });
                }
                else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    lastDuration = System.currentTimeMillis() - lastDown;

                    jsonObject = new JsonObject();
                    jsonObject.addProperty("text", "mdStop");

                    RetroAPI.APIService service = RetroAPI.createService(RetroAPI.APIService.class);
                    service.getVectors(jsonObject)
                            .enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    if (response.isSuccessful()) {

                                        try {
                                            msg = response.body().string();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        System.out.println("---TTTT :: POST msg from server :: " + msg);
//                                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                                    }
                                }
                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    System.out.println("---TTTT :: POST Throwable EXCEPTION:: " + t.getMessage());
                                }
                            });
                }
                return true;
            }

        });

        btnLeft.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    lastDown = System.currentTimeMillis();

                    jsonObject = new JsonObject();
                    jsonObject.addProperty("text", "motorLeft");

                    RetroAPI.APIService service = RetroAPI.createService(RetroAPI.APIService.class);
                    service.getVectors(jsonObject)
                            .enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    if (response.isSuccessful()) {

                                        try {
                                            msg = response.body().string();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        System.out.println("---TTTT :: POST msg from server :: " + msg);
//                                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                                    }
                                }
                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    System.out.println("---TTTT :: POST Throwable EXCEPTION:: " + t.getMessage());
                                }
                            });
                }
                else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    lastDuration = System.currentTimeMillis() - lastDown;

                    jsonObject = new JsonObject();
                    jsonObject.addProperty("text", "mlStop");

                    RetroAPI.APIService service = RetroAPI.createService(RetroAPI.APIService.class);
                    service.getVectors(jsonObject)
                            .enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    if (response.isSuccessful()) {

                                        try {
                                            msg = response.body().string();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        System.out.println("---TTTT :: POST msg from server :: " + msg);
//                                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                                    }
                                }
                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    System.out.println("---TTTT :: POST Throwable EXCEPTION:: " + t.getMessage());
                                }
                            });
                }
                return true;
            }

        });

        btnRight.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    lastDown = System.currentTimeMillis();

                    jsonObject = new JsonObject();
                    jsonObject.addProperty("text", "motorRight");

                    RetroAPI.APIService service = RetroAPI.createService(RetroAPI.APIService.class);
                    service.getVectors(jsonObject)
                            .enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    if (response.isSuccessful()) {

                                        try {
                                            msg = response.body().string();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        System.out.println("---TTTT :: POST msg from server :: " + msg);
//                                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                                    }
                                }
                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    System.out.println("---TTTT :: POST Throwable EXCEPTION:: " + t.getMessage());
                                }
                            });
                }
                else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    lastDuration = System.currentTimeMillis() - lastDown;

                    jsonObject = new JsonObject();
                    jsonObject.addProperty("text", "mrStop");

                    RetroAPI.APIService service = RetroAPI.createService(RetroAPI.APIService.class);
                    service.getVectors(jsonObject)
                            .enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    if (response.isSuccessful()) {

                                        try {
                                            msg = response.body().string();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        System.out.println("---TTTT :: POST msg from server :: " + msg);
//                                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                                    }
                                }
                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    System.out.println("---TTTT :: POST Throwable EXCEPTION:: " + t.getMessage());
                                }
                            });
                }
                return true;
            }

        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    lastDown = System.currentTimeMillis();

                    jsonObject = new JsonObject();
                    jsonObject.addProperty("text", "motorsSTOP");

                    RetroAPI.APIService service = RetroAPI.createService(RetroAPI.APIService.class);
                    service.getVectors(jsonObject)
                            .enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    if (response.isSuccessful()) {

                                        try {
                                            msg = response.body().string();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        System.out.println("---TTTT :: POST msg from server :: " + msg);
//                                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                                    }
                                }
                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    System.out.println("---TTTT :: POST Throwable EXCEPTION:: " + t.getMessage());
                                }
                            });
                }

            });

    }
}

