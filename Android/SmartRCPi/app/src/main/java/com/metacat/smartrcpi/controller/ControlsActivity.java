package com.metacat.smartrcpi.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.metacat.smartrcpi.R;
import com.metacat.smartrcpi.facerecognition.FaceRecognitionActivity;

public class ControlsActivity extends AppCompatActivity {

    private Button controllerButton;
    private Button cameraButton;
    private Button webViewButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.control);

        controllerButton = (Button) findViewById(R.id.controllerButon);
        cameraButton = (Button) findViewById(R.id.cameraButton);
        webViewButton = (Button) findViewById(R.id.webViewButton);


        controllerButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                // Start NewActivity.class
                Intent myIntent = new Intent(ControlsActivity.this,
                        ControllerActivity.class);
                startActivity(myIntent);
            }
        });

        cameraButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                // Start NewActivity.class
                Intent myIntent = new Intent(ControlsActivity.this,
                        FaceRecognitionActivity.class);
                startActivity(myIntent);
            }
        });
    }
}
