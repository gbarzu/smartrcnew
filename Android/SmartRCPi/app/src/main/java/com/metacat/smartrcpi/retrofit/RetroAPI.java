package com.metacat.smartrcpi.retrofit;

import com.google.gson.JsonObject;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;


public class RetroAPI {

    public interface APIService {

        @GET("/users/{user}")
        public Call<ResponseBody> greetUser(
                @Path("user") String username);

        @Headers("Content-type: application/json")

        @POST("/api/post")
        public Call<ResponseBody> getVectors(
                @Body JsonObject body);
    }

//    OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
//    Retrofit retrofit = new Retrofit.Builder()
//            .baseUrl("http://10.0.1.9:5000")
//            .addConverterFactory(GsonConverterFactory.create())
//            .client(httpClient.build())
//            .build();


//    APIService service = retrofit.create(APIService.class);

    private static Retrofit.Builder builder
            = new Retrofit.Builder()
            .baseUrl("http://10.0.1.9:5000")
            .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = builder.build();

    private static OkHttpClient.Builder httpClient
            = new OkHttpClient.Builder();

    public static <S> S createService(Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }
}

