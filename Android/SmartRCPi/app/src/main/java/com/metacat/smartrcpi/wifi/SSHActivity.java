package com.metacat.smartrcpi.wifi;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.metacat.smartrcpi.R;
import com.metacat.smartrcpi.controller.ControlsActivity;

import java.io.ByteArrayOutputStream;
import java.util.Properties;

public class SSHActivity extends AppCompatActivity {

    private TextView outputTextView;
    private Button controlsButton;
    private Button refreshButton;
    private static ByteArrayOutputStream baos;
    private String outputText;
    private static final String COMMAND = "./startServer.sh";
    private static final String HOST_NAME = "10.0.1.9";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ssh);

        new AsyncTask<Integer, Void, Void>(){
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                outputTextView = (TextView) findViewById(R.id.outputTextVIew);
            }

            @Override
            protected Void doInBackground(Integer... params) {
                try {
                    outputText = executeRemoteCommand("pi", "asdfasdf","10.0.1.9", 22, COMMAND);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                outputTextView.setText(outputText);
            }
        }.execute(1);
        controlsButton = (Button) findViewById(R.id.controlsButton);

        controlsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                // Start NewActivity.class
                Intent myIntent = new Intent(SSHActivity.this,
                        ControlsActivity.class);
                startActivity(myIntent);
            }
        });
    }

    public static String executeRemoteCommand(String username,String password,String hostname,int port,String command)
            throws Exception {
        JSch jsch = new JSch();
        Session session = jsch.getSession(username, hostname, port);
        session.setPassword(password);

        // Avoid asking for key confirmation
        Properties prop = new Properties();
        prop.put("StrictHostKeyChecking", "no");
        session.setConfig(prop);


        session.connect();

        // SSH Channel
        ChannelExec channelssh = (ChannelExec)
                session.openChannel("exec");
        baos = new ByteArrayOutputStream();
        channelssh.setOutputStream(baos);
        channelssh.setCommand(command); // this is where the RPi Flash server starts

        System.out.println("Command succeded");

        channelssh.connect();
        Thread.sleep(5000); // sleep to wait for the python server to start
        channelssh.disconnect();

        System.out.println("Output is" + baos.toString());
        return baos.toString();
    }

    public static ByteArrayOutputStream getBaos() {
        return baos;
    }

    public static void setBaos(ByteArrayOutputStream baos) {
        SSHActivity.baos = baos;
    }

}