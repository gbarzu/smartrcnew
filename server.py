from __future__ import print_function
from flask import Flask, render_template, request, jsonify
from dual_mc33926_rpi import motors, MAX_SPEED

import datetime
import time

import socket
import fcntl
import struct

app = Flask(__name__)

test_forward_speeds = list(range(0, MAX_SPEED, 1)) + \
  [MAX_SPEED] * 200 + list(range(MAX_SPEED, 0, -1)) + [0]

test_reverse_speeds = list(range(0, -MAX_SPEED, -1)) + \
  [-MAX_SPEED] * 200 + list(range(-MAX_SPEED, 0, 1)) + [0]

motorThrottle = motors.motor1
motorDirection = motors.motor2

currentMotorThrottleSpeed = 0
currentMotorDirectionSpeed = 0

motor_throttle_is_pressed = False
motor_direction_is_pressed = True

def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(s.fileno(),
                                        0x8915,
                                        struct.pack('256s',
                                                    bytes(ifname[:15],
                                                          'utf-8'))[20:24]))


def get_ip_address_bash(netdev='wlan0'):
    # Use ip addr show
    import subprocess
    arg = 'ip addr show ' + netdev
    p = subprocess.Popen(arg, shell=True, stdout=subprocess.PIPE)
    data = p.communicate()
    sdata = data[0].split(b'\n')
    macaddr = sdata[1].strip().split(b' ')[1]
    ipaddr = sdata[2].strip().split(b' ')[1].split(b'/')[0]
    return ipaddr


ip_addr = get_ip_address_bash().decode('utf-8')

print ('Local WLAN0 IP ADDRESS is: {}'.format(ip_addr))

@app.route("/")
def init():
    now = datetime.datetime.now()
    timeString = now.strftime("%Y-%m-%d %H:%M")

    motors.enable()
    motors.setSpeeds(0, 0)
    # turn_right = motors.motor2.setSpeed(50)
    # turn_left = motors.motor2.setSpeed(-50)
    # accelerate = motors.motor1.setSpeed(50)
    # decelerate = motors.motor1.setSpeed(-50)

    motorThrottleSts = motors.motor1
    motorDirectionSts = motors.motor2


    throtle_stop = motors.motor1.setSpeed(0)
    direction_stop = motors.motor2.setSpeed(0)

    templateData = {
        'title' : 'Motor status',
        'time': timeString,
        'motorThrottle': motorThrottleSts,
        'motorDirection': motorDirectionSts
        # 'motor_up_right': turn_right,
        # 'motor_up_left': turn_left,
        # 'acc': accelerate,
        # 'dcc': decelerate
    }
    return render_template('index.html', **templateData)

@app.route("/<deviceName>/<action>")
def action(deviceName, action, motorThrottle=None, motorDirection=None):
    motorThrottle = motors.motor1
    motorDirection = motors.motor2

    if deviceName == 'motorThrottle':
        actuator = motorThrottle
    if deviceName == 'motorDirection':
        actuator = motorDirection

    if action == "on":
        if actuator is motorDirection:
            actuator.setSpeed(300)
        else:
            actuator.setSpeed(200)
        time.sleep(0.3)
        actuator.setSpeed(0)
    if action == "off":
        actuator.setSpeed(0)

    now = datetime.datetime.now()
    timeString = now.strftime("%Y-%m-%d %H:%M")

    templateData = {
        'title': 'Actual Motor Test!',
        'time': timeString,
        'motorThrottleUp': motorThrottle,
        'motorThrottleBack'
        'motorDirection': motorDirection
    }

    return render_template('index.html', **templateData)

# POST
@app.route('/api/post', methods=['POST'])
def get_text_prediction():
    """
    #predicts requested text whether it is ham or spam
    #:return: json
    """
    motors.enable()
    motorThrottle = motors.motor1
    motorDirection = motors.motor2
    motorThrottle.setSpeed(0)
    motorDirection.setSpeed(0)
    time.sleep(0.1)


    json = request.get_json()
    print("JSON request is: {}".format(json))
    if len(['text']) == 0:
        return jsonify({'error': 'invalid input'})

    if 'motorUp' in json['text']:
        for s in range(0, 120):
            motorThrottle.setSpeed(s)
            time.sleep(0.005)

    if 'muStop' in json['text']:
        motorThrottle.setSpeed(0)
        motorDirection.setSpeed(0)

    if 'motorDown' in json['text']:
        for s in range(120, -1):
            motorThrottle.setSpeed(s)
            time.sleep(0.005)

    if 'mdStop' in json['text']:
        motorThrottle.setSpeed(0)
        motorDirection.setSpeed(0)

    if 'motorRight' in json['text']:
        for s in range(0, 120):
            motorThrottle.setSpeed(s)
            motorDirection.setSpeed(s)
            time.sleep(0.005)

    if 'motorLeft' in json['text']:
        for s in range(0, 120):
            motorThrottle.setSpeed(s)
            motorDirection.setSpeed(s * -1)
            time.sleep(0.005)

    if 'mrStop' in json['text']:
        motorThrottle.setSpeed(0)
        motorDirection.setSpeed(0)

    if 'mlStop' in json['text']:
        motorThrottle.setSpeed(0)
        motorDirection.setSpeed(0)

    return jsonify({'You pressed the button': json['text']})


if __name__ == "__main__":
   app.run(host=str(ip_addr), port=5000, debug=True)

# if 'motorUp' in json['text']:
#     global currentMotorDirectionSpeed
#     global currentMotorThrottleSpeed
#     global motor_throttle_is_pressed
#     global motor_direction_is_pressed
#
#     working_direction_speeds = [-200, 200]
#     '''
#     This is for the case when left or right buttons are already pressed
#     '''
#
#     if motor_direction_is_pressed is True:
#         if currentMotorDirectionSpeed in range(-360, 0, -1) or \
#                 currentMotorDirectionSpeed in range(0, 360, 1):
#             for speed_1 in range(0, 120):
#                 motorThrottle.setSpeed(speed_1)
#                 currentMotorThrottleSpeed = speed_1
#                 motor_throttle_is_pressed = True
#                 time.sleep(0.05)
#     else:
#         for speed_2 in range(0, 480):
#             motorThrottle.setSpeed(speed_2)
#             currentMotorThrottleSpeed = speed_2
#             motor_throttle_is_pressed = True
#             time.sleep(0.05)
#
# if 'muStop' in json['text']:
#     global currentMotorThrottleSpeed
#     global motor_throttle_is_pressed
#
#     motorThrottle.setSpeed(0)
#     currentMotorThrottleSpeed = 0
#     motor_throttle_is_pressed = False
#
# if 'motorDown' in json['text']:
#     global currentMotorDirectionSpeed
#     global currentMotorThrottleSpeed
#     global motor_throttle_is_pressed
#     global motor_direction_is_pressed
#
#     if motor_direction_is_pressed is True:
#         if currentMotorDirectionSpeed in range(-360, 0, -1) or \
#                 currentMotorDirectionSpeed in range(0, 360, 1):
#             for speed_1 in range(-120, 0):
#                 motorThrottle.setSpeed(speed_1)
#                 currentMotorThrottleSpeed = speed_1
#                 time.sleep(0.05)
#     else:
#         for speed_2 in range(0, -360, -1):
#             motorThrottle.setSpeed(speed_2)
#             currentMotorThrottleSpeed = speed_2
#             time.sleep(0.05)
#
#     # motorThrottle.setSpeed(-200)
#     motor_throttle_is_pressed = True
#
# elif 'mdStop' in json['text']:
#     global currentMotorThrottleSpeed
#     global motor_throttle_is_pressed
#
#     motorThrottle.setSpeed(0)
#     currentMotorThrottleSpeed = 0
#     motor_throttle_is_pressed = False
#
# if 'motorLeft' in json['text']:
#     global currentMotorDirectionSpeed
#     global currentMotorThrottleSpeed
#     global motor_throttle_is_pressed
#     global motor_direction_is_pressed
#
#     if motor_throttle_is_pressed is True:
#         if currentMotorThrottleSpeed in range(0, -120, -1) or \
#                 currentMotorThrottleSpeed in range(0, 120, 1):  # be sure to check if Throttle doesn't exceed 120
#             for speed in range(0, -360, -1):
#                 motorDirection.setSpeed(speed)
#                 time.sleep(0.05)
#                 currentMotorDirectionSpeed = speed
#                 motor_direction_is_pressed = True
#
#         elif currentMotorThrottleSpeed in range(-120, -480, -1):
#             for speed in range(-480 - currentMotorThrottleSpeed, -120, 1):
#                 motorThrottle.setSpeed(speed)
#                 time.sleep(0.001)
#                 currentMotorThrottleSpeed = speed
#
#             for speed in range(0, -360, -1):
#                 motorDirection.setSpeed(speed)
#                 time.sleep(0.05)
#                 currentMotorDirectionSpeed = speed
#                 motor_direction_is_pressed = True
#
#         elif currentMotorThrottleSpeed in range(120, 480, 1):
#
#             for speed in range(120, 480 - currentMotorThrottleSpeed, 1):
#                 motorThrottle.setSpeed(speed)
#                 time.sleep(0.001)
#                 currentMotorThrottleSpeed = speed
#
#             for speed in range(0, -360, -1):
#                 motorDirection.setSpeed(speed)
#                 time.sleep(0.05)
#                 currentMotorDirectionSpeed = speed
#                 motor_direction_is_pressed = True
#     else:
#         for speed in range(0, -360, -1):
#             motorThrottle.setSpeed(speed)
#             currentMotorThrottleSpeed = speed
#             time.sleep(0.05)
#             motor_direction_is_pressed = True
#
#     # motorDirection.setSpeed(-200)
#
#
#
#
#
# elif 'mlStop' in json['text']:
#     global currentMotorDirectionSpeed
#     global motor_direction_is_pressed
#
#     motorDirection.setSpeed(0)
#     currentMotorDirectionSpeed = 0
#     motor_direction_is_pressed = False
#
# if 'motorRight' in json['text']:
#     global currentMotorDirectionSpeed
#     global currentMotorThrottleSpeed
#     global motor_throttle_is_pressed
#     global motor_direction_is_pressed
#
#     if motor_throttle_is_pressed is True:
#         if currentMotorThrottleSpeed in range(0, -120, -1) or \
#                 currentMotorThrottleSpeed in range(0, 120, 1):  # be sure to check if Throttle doesn't exceed 120
#             for speed in range(0, 360, 1):
#                 motorDirection.setSpeed(speed)
#                 time.sleep(0.05)
#                 currentMotorDirectionSpeed = speed
#                 motor_direction_is_pressed = True
#
#         elif currentMotorThrottleSpeed in range(-120, -480, -1):
#             for speed in range(-480 - currentMotorThrottleSpeed, -120, 1):
#                 motorThrottle.setSpeed(speed)
#                 time.sleep(0.001)
#                 currentMotorThrottleSpeed = speed
#
#             for speed in range(0, 360, 1):
#                 motorDirection.setSpeed(speed)
#                 time.sleep(0.05)
#                 currentMotorDirectionSpeed = speed
#                 motor_direction_is_pressed = True
#
#         elif currentMotorThrottleSpeed in range(120, 480, 1):
#
#             for speed in range(120, 480 - currentMotorThrottleSpeed, 1):
#                 motorThrottle.setSpeed(speed)
#                 time.sleep(0.001)
#                 currentMotorThrottleSpeed = speed
#
#             for speed in range(0, 360, 1):
#                 motorDirection.setSpeed(speed)
#                 time.sleep(0.05)
#                 currentMotorDirectionSpeed = speed
#                 motor_direction_is_pressed = True
#     else:
#         for speed in range(0, 480, 1):
#             motorThrottle.setSpeed(speed)
#             currentMotorThrottleSpeed = speed
#             time.sleep(0.05)
#             motor_direction_is_pressed = True
#     # motorDirection.setSpeed(200)
#
# elif 'mrStop' in json['text']:
#     global currentMotorDirectionSpeed
#     global motor_direction_is_pressed
#
#     motorDirection.setSpeed(0)
#     currentMotorDirectionSpeed = 0
#     motor_direction_is_pressed = False