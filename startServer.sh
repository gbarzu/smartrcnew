# Kill the flask server if it was already started !
sudo kill -9 $(ps aux | grep '[s]erver.py' | awk '{print $2}')

cd /home/pi/rpiWebServer
sudo python3 server.py

echo 'Server started!'
